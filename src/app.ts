import { AxiosInstance, AxiosResponse, default as axios } from 'axios';
import { promises as fs } from 'fs';
import * as inquirer from 'inquirer';
import { Project } from './models';
import { cloneProject, pathExists } from './utils';

interface ConfigData {
    accessToken: string;
    baseUrl: string;
    cloneType?: 'All' | 'Starred only';
    filter?: string;
    forceClone?: boolean;
}

main().then().catch(e => {
    console.error('😱 error occured with ferry\n', e);
});

async function main(): Promise<void> {
    console.log('welcome to Ferry 🎉');

    const configFileName: string = './ferry.json';

    let configData: ConfigData = { accessToken: '', baseUrl: '' };

    let configExists: boolean = await pathExists(configFileName);
    if (configExists) {
        console.log(`"${configFileName}" file found reading and parsing it to json...`);
        let configContent: string = await fs.readFile(configFileName, { encoding: 'utf8' });
        try {
            configData = JSON.parse(configContent) as ConfigData;
            console.log(`"${configFileName}" file loaded with content:\n`, configData);
        } catch (error) {
            console.log(`error eccured when reading and parsing "${configFileName}"`, error);
            return;
        }
    } else {
        console.log(`no config file found \nyou can create a "${configFileName}" to store config data with the struct:\n`, configData);
    }

    let inputConfig: ConfigData = await inquirer.prompt([{
        type: 'input',
        message: 'please input your GitLab URL:',
        name: 'baseUrl',
        default: configData.baseUrl || 'https://gitlab.com/api/v4',
    }, {
        type: 'input',
        message: 'please input your access token:',
        name: 'accessToken',
        default: configData.accessToken || undefined,
        validate: f => {
            if (!f) {
                return 'please DO provide an access token';
            } else {
                return true;
            }
        },
    }, {
        type: 'list',
        message: 'please select repos to clone:',
        name: 'cloneType',
        choices: ['All', 'Starred only'],
        default: configData.cloneType,
    }, {
        type: 'input',
        message: 'please input filter for repo path:',
        name: 'filter',
        default: configData.filter || undefined,
    }, {
        type: 'confirm',
        message: 'Force clone by deleting existing folders?',
        name: 'forceClone',
        default: false,
    }]);

    configData = { ...configData, ...inputConfig };

    console.log('current working config:\n', configData);
    console.log(`Repos will be clone into current folder: ${process.cwd()}`);

    let ready: { go: boolean } = await inquirer.prompt([{
        type: 'confirm',
        message: 'Ready to go?',
        name: 'go',
    }]);
    if (!ready.go) {
        return;
    }

    console.log(`saving current config to file "${configFileName}" ...`);
    await fs.writeFile(configFileName, JSON.stringify(configData, null, 4), { encoding: 'utf8' });

    console.log('listing projects...');
    let axInstance: AxiosInstance = axios.create({
        baseURL: configData.baseUrl,
        headers: { 'Private-Token': configData.accessToken },
    });

    let projectList: Array<Project> = new Array<Project>();
    let page: number = 1;
    let starredOnly: boolean = configData.cloneType == 'Starred only';
    while (page >= 0) {
        let projectRes: AxiosResponse<Array<Project>> = await axInstance.get<Array<Project>>(`projects?order_by=id&sort=asc&starred=${starredOnly}&per_page=100&page=${page}`);
        page = parseInt((projectRes.headers as Record<string, string>)['x-next-page'], 10);
        projectList.push(...projectRes.data);
    }

    if (projectList.length < 1) {
        console.log('no project found');
        return;
    } else {
        console.log(configData.cloneType, 'projects count:', projectList.length);
    }

    let filteredProjects: Array<Project> = projectList.filter(p => configData.filter ? p.path_with_namespace.indexOf(configData.filter) == 0 : true);
    if (filteredProjects.length < 1) {
        console.log('no project matches the filter for path:', configData.filter);
        return;
    } else {
        console.log('filtered projects count:', filteredProjects.length);
    }

    console.log('starting clone projects...');
    for (let i: number = 0; i < filteredProjects.length; i++) {
        let currentProject: Project = filteredProjects[i];
        console.log(`processing project "${currentProject.name_with_namespace.replace(/ /g, '')}" (${i + 1}/${filteredProjects.length})...`);
        await cloneProject(currentProject, configData.forceClone);
    }
    console.log('Goodbye 👋');
}
