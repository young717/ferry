import { exec } from 'child_process';
import { PathLike, promises as fs } from 'fs';
import util from 'util';
import { Project } from './models';

// tslint:disable-next-line: typedef
export const execAsync = util.promisify(exec);

export interface ExecOutput {
    stdout: string;
    stderr: string;
}

export async function pathExists(path: PathLike): Promise<boolean> {
    try {
        await fs.access(path);
        return true;
    } catch {
        return false;
    }
}

export async function folderFilled(path: PathLike): Promise<boolean> {
    try {
        let files: Array<string> = await fs.readdir(path);
        return files.length != 0;
    } catch (error) {
        return false;
    }
}

export async function cloneProject(project: Project, force: boolean = false): Promise<void> {
    let projectName: string = project.name_with_namespace.replace(/ /g, '');
    let isFolderFilled: boolean = await folderFilled(project.path_with_namespace);
    if (isFolderFilled) {
        if (force) {
            console.log(`deleting "${project.path_with_namespace}" for force cloning ...`);
            await fs.rmdir(project.path_with_namespace, { recursive: true });
        } else {
            console.log(`skipped since folder: "${project.path_with_namespace}" is not empty`);
            return;
        }
    }

    try {
        console.log(`cloning project "${projectName}" into "./${project.path_with_namespace}" ...`);
        await execAsync(`git clone ${project.http_url_to_repo} ${project.path_with_namespace}`);
        console.log('done');
    } catch (error) {
        console.log(`error occured when cloning project: "${projectName}"`, error);
    }
}
